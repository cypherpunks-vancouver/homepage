---
date: 2019-02-12
title: About
type: page
---

We are the Cypherpunks of Vancouver.

We are a collective of hackers who wish to improve society through the use of cryptography and personal security.  We value personalization, customization and privacy.

# Who We Are
We are actively recruiting community members.

So far we are:

- Lucas Amorim \[[web](https://www.lucasamorim.ca)\] \[[activity pub](https://mastodon.club/@lucasamorim)\]
- Aaron Oman \[[web](https://www.groovestomp.com)\] \[[activity pub](https://pleroma.groovestomp.com/GrooveStomp)\]